package com.khannajai.list;

public class List
{
    private static final int INIT_LEN = 10;
    private Object [] items;
    private int numItems;
    private int size = INIT_LEN;
    private int currentObject;

    public List()
    {
        items = new Object[INIT_LEN];
        numItems = 0;
    }

    public void AddToEnd(Object ob)
    {
        if (numItems == size)
        {
            size = numItems * 2;
            Object [] new_items = new Object[size];
            System.arraycopy(items, 0, new_items, 0, numItems);
            items = new_items;
        }
        numItems++;
        items[numItems - 1] = ob;
    }

    public void firstElement()
    {
        currentObject = 0;
    }

    public Object nextElement()
    {
        Object item = items[currentObject];
        currentObject++;
        return item;
    }

    public boolean hasMoreElements()
    {
        if (numItems > 0 && currentObject < numItems)
        {
            return true;
        }
        else return false;
    }

    public void Print()
    {
        System.out.print("[");
        for (int i = 0; i < numItems; i++)
        {
            if (i == numItems - 1)
            {
                System.out.print(items[i]);
            }
            else System.out.print(items[i] + " ");
        }
        System.out.println("]");
    }

    // public static void main(String args[])
    // {
    //     List my_list = new List();
    //     my_list.AddToEnd(Integer.valueOf(1));
    //     my_list.AddToEnd(Integer.valueOf(2));
    //     my_list.AddToEnd(new String("hello"));
    //     for (int i = 0; i < 25; i++)
    //     {
    //         my_list.AddToEnd(Integer.valueOf(i));
    //     }
    //     my_list.Print();
    // }

    // public static void main(String[] args)
    // {
    //     // create a list of strings
    //     List L = new List();
    //     for (int k = 0; k < args.length; k++) {
    //         L.AddToEnd(args[k]);
    //     }

    //     // now iterate through the list, printing each item
    //     L.firstElement();
    //     while (L.hasMoreElements()) {
    //         String tmp = (String)L.nextElement();
    //     System.out.println(tmp);
    //     }
    // }
}