package com.khannajai.list;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class ListTest 
{
    /**
     * Rigorous Test :-)
     */
    @Test
    public void shouldAnswerWithTrue()
    {
        assertTrue( true );
    }

    public void testListAddToEnd()
    {
        List my_list = new List();
        my_list.AddToEnd(Integer.valueOf(25));
        my_list.firstElement();
        int element = 0;
        if(my_list.hasMoreElements())
        {
            element = ((Integer)my_list.nextElement()).intValue();
        }
        assertTrue(element == 25);
    }
}
