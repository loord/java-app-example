# Java App

This is an example of a simple Java application that is built into an executable JAR using Maven.

## Build

```bash
mvn clean compile package
```

## Run

```bash
java -jar target/app-1.0-SNAPSHOT.jar
```

## Build and run in Docker

```bash
docker build -t app .
docker run --rm -it app:latest
```
